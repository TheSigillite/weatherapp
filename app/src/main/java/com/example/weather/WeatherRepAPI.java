package com.example.weather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherRepAPI {
    @GET("/data/2.5/weather")
    Call<WeatherRep> getWeather(@Query("q") String q  , @Query("APPID") String APPID, @Query("units") String units);

    //@GET("/data/2.5/weather?q=Tarnow,pl&APPID=749561a315b14523a8f5f1ef95e45864&units=metric")
    //Call<WeatherRep> getWeather();
}
