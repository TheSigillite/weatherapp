package com.example.weather;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText ci = findViewById(R.id.CityField);
        SharedPreferences sharedPreferences = getSharedPreferences("shared pref",MODE_PRIVATE);
        String cit = sharedPreferences.getString("CITY_OK","");
        ci.setText(cit);
    }

    public boolean networkCheck(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if(activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }
        else{
            Context context = getApplicationContext();
            Toast nonettoast = Toast.makeText(context,"Brak połącznia z internetem. Sprawdź połączenie.",Toast.LENGTH_LONG);
            nonettoast.setGravity(Gravity.TOP,0,0);
            nonettoast.show();
            return false;
        }

    }


    public void WeatherTimeee(View view) {
        EditText et = findViewById(R.id.CityField);
        String city = et.getText().toString();
        if(!city.equals("") && networkCheck()) {
            Intent passer = new Intent(getApplicationContext(), WeatherDisplay.class);
            passer.putExtra("city",city);
            startActivityForResult(passer, 69);
        }

    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data){
        Context context = getApplicationContext();
        Toast errtoast = Toast.makeText(context,"Nieznany Blad. Zkontaktuj sie z dewelopem",Toast.LENGTH_LONG);
        errtoast.setGravity(Gravity.TOP,0,0);
        if(requestCode==69 && resultCode==RESULT_OK){
            Integer result = data.getIntExtra("pepega",200);
            if(result == 404){
                errtoast.setText("Nieznana lub nieobslugiwana lokacja");
                errtoast.show();
            }
            else if(result == -400){
                errtoast.show();
            }
        }
        if(requestCode==69 && resultCode==RESULT_CANCELED){
            Throwable throwable = (Throwable) data.getSerializableExtra("except");
            errtoast.setText(throwable.toString()+"\nZkontaktuj sie z developerami");
            errtoast.show();

        }
    }

}
