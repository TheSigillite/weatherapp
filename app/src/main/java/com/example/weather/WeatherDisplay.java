package com.example.weather;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.Serializable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherDisplay extends Activity {

    String city;
    SwipeRefreshLayout swipeRefresh;
    WeatherRep weatherRep;

    Thread thread = new Thread(){
        @Override
        public void run(){
            try {
                while (!thread.isInterrupted()){
                    Thread.sleep(300000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            serviceWeather();
                        }
                    });
                }
            }catch (InterruptedException e){

            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_display);
        Intent getter = getIntent();
        swipeRefresh = findViewById(R.id.swipeRefresh);
        city = getter.getStringExtra("city");
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                serviceWeather();
                swipeRefresh.setRefreshing(false);
            }
        });
        serviceWeather();
        thread.start();
    }

    //using picasso library for image
    public void isWeather(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        WeatherRepAPI repAPI = retrofit.create(WeatherRepAPI.class);
        Call<WeatherRep> call = repAPI.getWeather(city+",pl","749561a315b14523a8f5f1ef95e45864","metric");
        //Call<WeatherRep> call = repAPI.getWeather();
        call.enqueue(new Callback<WeatherRep>(){
            @Override
            public void onResponse(@NonNull Call<WeatherRep> call, @NonNull Response<WeatherRep> response){
                if(response.code()==200) {
                    weatherRep = response.body();
                    assert weatherRep != null;
                    setDisplays();
                    SharedPreferences sharedPreferences = getSharedPreferences("shared pref",MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("CITY_OK",city);
                    editor.apply();

                } else if(response.code()==404) {
                    Intent i = new Intent();
                    i.putExtra("pepega",404);
                    setResult(RESULT_OK,i);
                    finish();
                } else{
                    Intent i = new Intent();
                    i.putExtra("pepega",-400);
                    setResult(RESULT_OK,i);
                    finish();
                }
            }
            @Override
            public void onFailure(@NonNull Call<WeatherRep> call,@NonNull Throwable t){
                Intent xd = new Intent();
                xd.putExtra("except",(Serializable) t);
                setResult(RESULT_CANCELED,xd);
                finish();
            }
        });
    }

    private void serviceWeather(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if(activeNetwork != null && activeNetwork.isConnected()){
            isWeather();
        }
        else {
            Context context = getApplicationContext();
            Toast nonettoast = Toast.makeText(context,"Brak połącznia z internetem. Sprawdź połączenie.",Toast.LENGTH_LONG);
            nonettoast.setGravity(Gravity.TOP,0,0);
            nonettoast.show();
        }
    }

    private void setDisplays(){
        TextView tem = findViewById(R.id.Tempe);
        TextView pres = findViewById(R.id.PSI);
        TextView hum = findViewById(R.id.Humid);
        TextView temmax = findViewById(R.id.maxtemp);
        TextView temmin = findViewById(R.id.mintemp);
        ImageView wimage = findViewById(R.id.weatherimage);
        tem.setText(weatherRep.main.temp.toString()+" C");
        pres.setText(weatherRep.main.pressure.toString()+" PSI");
        hum.setText(weatherRep.main.humidity.toString());
        temmax.setText(weatherRep.main.tempMax.toString()+" C");
        temmin.setText(weatherRep.main.tempMin.toString()+" C");
        String ur = "http://openweathermap.org/img/w/"+weatherRep.weather.get(0).icon+".png";
        Picasso.get().load(ur).into(wimage);
    }



    @Override
    public void onBackPressed(){
        Intent i = new Intent();
        setResult(RESULT_OK,i);
        finish();
    }




}
